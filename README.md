Insults.Fyi
===

This is the source code for <https://insults.fyi>.

To start the server, run

``` sh
pip install -r requirements.txt
curl -LO https://s.lyb.rocks/predictor.pkl
python3 manage.py runserver
```

Training code for the machine learning model (`predictor.pkl`) and the relevant training data are in the directory `model/`.

Related projects for training data generation:

- <https://github.com/bearrrrrrro/LawHackson>: Extracts keywords from judgements
- <https://github.com/108062138/lawSunShineCrawle>: Crawls background information about the judges
