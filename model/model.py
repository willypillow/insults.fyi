# pylint: disable=missing-module-docstring
# pylint: disable=missing-function-docstring,missing-class-docstring
# pylint: disable=invalid-name
# pylint: disable=too-many-lines
# pylint: disable=unused-import
import copy
import csv
import pprint
import sys
import typing

from timeit import default_timer as timer

import catboost
import scipy
import skorch
import torch

import cloudpickle as pickle
import lightgbm as lgbm
import numpy as np
import xgboost as xgb

import pytorch_tabnet as tabnet
import pytorch_tabnet.tab_model

import sklearn as sk
from sklearn.experimental import enable_halving_search_cv
from sklearn.experimental import enable_hist_gradient_boosting
import sklearn.cluster
import sklearn.compose
import sklearn.dummy
import sklearn.ensemble
import sklearn.feature_selection
import sklearn.gaussian_process
import sklearn.impute
import sklearn.inspection
import sklearn.kernel_ridge
import sklearn.linear_model
import sklearn.mixture
import sklearn.model_selection
import sklearn.neural_network
import sklearn.pipeline
import sklearn.utils

from tpot.builtins import StackingEstimator

try:
    # Imports for development
    import autosklearn
    import hpsklearn
    import hyperopt
    import optuna
    import shap

    import pandas as pd
    import matplotlib.pyplot as plt

    import autogluon.core
    import supervised.automl

    from autogluon.tabular import TabularPredictor
    from statsmodels.stats.outliers_influence import variance_inflation_factor

    class AutoML(supervised.automl.AutoML):
        def __init__(self, data_headers, *args, **kwargs):
            self.data_headers = data_headers
            supervised.automl.AutoML.__init__(self, *args, **kwargs)

        def fit(self, X, y, *args, **kwargs):
            return supervised.automl.AutoML.fit(self, X.astype(float),
                                                y.astype(float), *args,
                                                **kwargs)

        def predict(self, X, **kwargs):
            X = pd.DataFrame(X, columns=self.data_headers)
            return supervised.automl.AutoML.predict(self, X.astype(float),
                                                    **kwargs)

except ImportError as e:
    print('Error while importing dev packages:\n', e, '\n', '=' * 80)

y_transform = None


def real_mse_loss(y_true, y_pred):
    y_true = y_transform.inverse_transform(y_true.reshape(-1, 1)).reshape(-1)
    y_pred = y_transform.inverse_transform(y_pred.reshape(-1, 1)).reshape(-1)
    return -np.sum((y_true - y_pred)**2)


def real_mse_loss_df(y_true, y_pred):
    if isinstance(y_true, pd.Series):
        y_true = y_true.to_numpy()
    if isinstance(y_pred, pd.Series):
        y_pred = y_pred.to_numpy()
    return real_mse_loss(y_true, y_pred)


class NearLoss(torch.nn.Module):
    @staticmethod
    def forward(yhat, y):
        # pylint: disable=no-member
        mae = torch.abs(yhat - y)
        diff = (y - yhat) / (yhat + 1e-3)
        mae[(diff < 1) & (diff > -0.5)] *= 0.01
        return torch.mean(mae)


class NNRegressor(skorch.NeuralNetRegressor):
    def fit(self, X, y, **kwargs):
        if y.ndim == 1:
            y = y.reshape(-1, 1)
        return skorch.NeuralNetRegressor.fit(self, X, y, **kwargs)


class TabNetRegressor(sk.base.RegressorMixin, sk.base.BaseEstimator):
    def __init__(self, kwargs=None):
        self.kwargs = kwargs if kwargs else {}
        self.clf = None

    def fit(self, X, y, init_kwargs=None, **kwargs):
        if init_kwargs is None:
            init_kwargs = self.kwargs
        else:
            init_kwargs = {**self.kwargs, **init_kwargs}

        self.clf = tabnet.tab_model.TabNetRegressor(**init_kwargs)
        if y.ndim == 1:
            y = y.reshape(-1, 1)

        if 'eval_set' not in kwargs:
            split = sk.model_selection.KFold(10)
            train_idx, valid_idx = next(iter(split.split(X)))
            valid_x, valid_y = X[valid_idx], y[valid_idx]
            train_x, train_y = X[train_idx], y[train_idx]

            return self.clf.fit(train_x,
                                train_y,
                                max_epochs=1000,
                                patience=50,
                                eval_set=[(valid_x, valid_y.reshape(-1, 1))],
                                **kwargs)

        return self.clf.fit(X, y, max_epochs=1000, patience=50, **kwargs)

    def predict(self, *args, **kwargs):
        return self.clf.predict(*args, **kwargs)


class AutogluonRegressor(sk.base.RegressorMixin, sk.base.BaseEstimator):
    def __init__(self, clf, data_headers):
        self.clf = clf
        self.data_headers = data_headers

    def fit(self, x, y, *args, **kwargs):
        df = pd.DataFrame(x, columns=self.data_headers)
        df['money'] = y

        if 'time_limit' not in kwargs:
            kwargs['time_limit'] = 12 * 60 * 60
        if 'presets' not in kwargs:
            kwargs['presets'] = ['best_quality']
        if 'num_bag_folds' not in kwargs:
            kwargs['num_bag_folds'] = 10
        if 'num_bag_sets' not in kwargs:
            kwargs['num_bag_sets'] = 40
        if 'refit_full' not in kwargs:
            kwargs['refit_full'] = True

        self.clf.fit(df, *args, **kwargs)
        return self

    def predict(self, x, *args, **kwargs):
        df = pd.DataFrame(x, columns=self.data_headers)
        return self.clf.predict(df,
                                as_pandas=False,
                                model='WeightedEnsemble_L3',
                                *args,
                                **kwargs)


class FixedRegressor(sk.base.TransformerMixin, sk.base.RegressorMixin,
                     sk.base.BaseEstimator):
    class Wrapper():
        # pylint: disable=too-few-public-methods
        def __init__(self, item):
            self.item = item

    def __init__(self, clf):
        if isinstance(clf, self.Wrapper):
            self.clf = clf
        else:
            self.clf = self.Wrapper(clf)

    def fit(self, *args, **kwargs):
        _ = args, kwargs  # Unused
        return self

    def predict(self, *args, **kwargs):
        return self.clf.item.predict(*args, **kwargs)

    def transform(self, *args, **kwargs):
        return self.clf.item.transform(*args, **kwargs)


class FixedStackingRegressor(sk.base.RegressorMixin, sk.base.BaseEstimator):
    def __init__(self, estimators, final_estimator):
        self.estimators = estimators
        self.final_estimator = final_estimator

    def fit(self, x, y):
        preds = [clf[1].predict(x).reshape(-1) for clf in self.estimators]
        final_feats = np.stack(preds, axis=1)
        self.final_estimator.fit(final_feats, y)
        return self

    def predict(self, x):
        preds = [clf[1].predict(x).reshape(-1) for clf in self.estimators]
        final_feats = np.stack(preds, axis=1)
        return self.final_estimator.predict(final_feats)


class DetentionTransform():
    def __init__(self):
        self.money_mean = 0
        self.money_std = 0
        self.detention_mean = 0
        self.detention_std = 0

    def fit(self, x, y):
        detention_indices = x[:, -1] > 0.5
        money_indices = x[:, -1] < 0.5
        self.money_mean = np.mean(y[money_indices])
        self.money_std = np.std(y[money_indices])
        self.detention_mean = np.mean(y[detention_indices])
        self.detention_std = np.std(y[detention_indices])
        return self

    def transform(self, x, y):
        y = y.copy()
        detention_indices = x[:, -1] > 0.5
        y[detention_indices] = (
            y[detention_indices] - self.detention_mean
        ) / self.detention_std * self.money_std + self.money_mean
        return x, y

    def inverse_transform(self, x, y):
        y = y.copy()
        detention_indices = x[:, -1] > 0.5
        y[detention_indices] = (
            y[detention_indices] - self.money_mean
        ) / self.money_std * self.detention_std + self.detention_mean
        return x, y


class DetentionTransformQuantile():
    def __init__(self):
        self.qt_money = sk.preprocessing.QuantileTransformer()
        self.qt_detention = sk.preprocessing.QuantileTransformer()

    def fit(self, x, y):
        self.qt_money = sk.preprocessing.QuantileTransformer().fit(
            y[x[:, -1] < 0.5, None])
        self.qt_detention = sk.preprocessing.QuantileTransformer().fit(
            y[x[:, -1] > 0.5, None])

    def transform(self, x, y):
        y = y.copy()
        y[x[:, -1] > 0.5] = self.qt_money.inverse_transform(
            self.qt_detention.transform(y[x[:, -1] > 0.5, None])).reshape(-1)
        y[x[:, -1] < 0.5] = self.qt_money.transform(y[x[:, -1] < 0.5,
                                                      None]).reshape(-1)
        y[x[:, -1] > 0.5] = self.qt_detention.transform(y[x[:, -1] > 0.5,
                                                          None]).reshape(-1)
        return y

    def inverse_transform(self, x, y):
        y[x[:, -1] < 0.5] = self.qt_money.inverse_transform(
            y[x[:, -1] < 0.5, None]).reshape(-1)
        y[x[:, -1] > 0.5] = self.qt_money.inverse_transform(
            y[x[:, -1] > 0.5, None]).reshape(-1)


class CustomFeatureTransform(sk.base.TransformerMixin, sk.base.BaseEstimator):
    # pylint: disable=too-many-instance-attributes
    def __init__(self):
        self.judge_transform = sk.preprocessing.QuantileTransformer(
            output_distribution='normal')
        self.words_transform = sk.preprocessing.QuantileTransformer(
            output_distribution='normal')
        self.year_transform = sk.preprocessing.QuantileTransformer(
            output_distribution='normal')
        self.bad_transform = sk.preprocessing.QuantileTransformer(
            output_distribution='normal')
        self.good_transform = sk.preprocessing.QuantileTransformer(
            output_distribution='normal')
        self.level_mean = []
        self.level_std = []
        self.loc_mean = []
        self.loc_std = []
        self.bad_idx = [11, 14, 18, 19, 20, 23, 24, 25, 26, 27, 28]
        self.good_idx = [16, 17, 21, 22]
        self.words_idx = slice(29, 109)
        self.judge_idx = 9
        self.year_idx = 110
        self.loc_ofst = 0
        self.level_ofst = 6

    def fit(self, x, y):
        self.level_mean = []
        self.level_std = []
        self.loc_mean = []
        self.loc_std = []
        x = x.copy()
        y = y.copy()
        for i in range(3):
            indices = x[:, self.level_ofst + i] > 0.5
            self.level_mean.append(np.mean(y[indices]))
            self.level_std.append(np.std(y[indices]))
        for i in range(6):
            indices = x[:, self.loc_ofst + i] > 0.5
            self.loc_mean.append(np.mean(y[indices]))
            self.loc_std.append(np.std(y[indices]))
        x[x[:, self.judge_idx] <= 0, self.judge_idx] = np.nan
        self.judge_transform.fit(x[:, self.judge_idx].reshape(-1, 1))
        num_words = np.sum(x[:, self.words_idx], axis=1)
        bad_count = np.sum(x[:, self.bad_idx], axis=1)
        good_count = np.sum(x[:, self.good_idx], axis=1)
        self.words_transform.fit(num_words.reshape(-1, 1))
        self.year_transform.fit(x[:, self.year_idx].reshape(-1, 1))
        self.bad_transform.fit(bad_count.reshape(-1, 1))
        self.good_transform.fit(good_count.reshape(-1, 1))

        return self

    def transform(self, x):
        x = x.copy()
        cases_notfound = x[:, self.judge_idx] < -0.5
        cases_zero = (x[:, self.judge_idx] > -0.5) & (x[:, self.judge_idx] <
                                                      0.5)
        num_words = np.sum(x[:, self.words_idx], axis=1)
        level_mean = np.zeros_like(x[:, 0])
        level_std = np.zeros_like(x[:, 0])
        loc_mean = np.zeros_like(x[:, 0])
        loc_std = np.zeros_like(x[:, 0])
        bad_count = np.sum(x[:, self.bad_idx], axis=1)
        good_count = np.sum(x[:, self.good_idx], axis=1)
        for i in range(3):
            indices = x[:, self.level_ofst + i] > 0.5
            level_mean[indices] = self.level_mean[i]
            level_std[indices] = self.level_std[i]
        for i in range(6):
            indices = x[:, self.loc_ofst + i] > 0.5
            loc_mean[indices] = self.loc_mean[i]
            loc_std[indices] = self.loc_std[i]

        x[x[:, self.judge_idx] <= 0, self.judge_idx] = np.nan
        x[:, self.judge_idx] = self.judge_transform.transform(
            x[:, self.judge_idx].reshape(-1, 1)).reshape(-1)

        num_words = self.words_transform.transform(num_words.reshape(
            -1, 1)).reshape(-1)
        bad_count = self.bad_transform.transform(bad_count.reshape(
            -1, 1)).reshape(-1)
        good_count = self.good_transform.transform(good_count.reshape(
            -1, 1)).reshape(-1)

        x[:, self.year_idx] = self.year_transform.transform(
            x[:, self.year_idx].reshape(-1, 1)).reshape(-1)

        x = np.concatenate([
            x[:, :-1],
            cases_notfound.astype(np.float32).reshape(-1, 1),
            cases_zero.astype(np.float32).reshape(-1, 1),
            num_words.reshape(-1, 1),
            level_mean.reshape(-1, 1),
            level_std.reshape(-1, 1),
            loc_mean.reshape(-1, 1),
            loc_std.reshape(-1, 1),
            bad_count.reshape(-1, 1),
            good_count.reshape(-1, 1),
            x[:, -1].reshape(-1, 1),
        ],
                           axis=1)

        return x


class FeatureStackTransform(sk.base.TransformerMixin, sk.base.BaseEstimator):
    def __init__(self, n_features):
        self.n_features = n_features

    def fit(self, x, y=None):
        _ = x, y  # Unused
        return self

    def transform(self, x):
        features = x[:, :-self.n_features]
        estimates = x[:, -self.n_features:]
        final_feat = features[:, :, np.newaxis] * estimates[:, np.newaxis, :]
        final_feat = final_feat.reshape(final_feat.shape[0], -1)
        return final_feat


class AttitudeTransform(sk.base.TransformerMixin, sk.base.BaseEstimator):
    def fit(self, x, y=None):
        _ = x, y  # Unused
        return self

    @staticmethod
    def transform(x):
        bg_idx = list(range(29)) + list(range(109, 113)) + list(range(
            114, 121))
        word_idx = list(range(29, 109)) + [113]
        assert len(bg_idx) + len(word_idx) == x.shape[1]
        bg = x[:, bg_idx]
        word = x[:, word_idx]
        final = bg[:, :, np.newaxis] * word[:, np.newaxis, :]
        final = final.reshape(final.shape[0], -1)
        return final


class Predictor():
    # pylint: disable=too-few-public-methods
    def __init__(self, detention_transform, y_trans, clf):
        self.detention_transform = detention_transform
        self.y_transform = y_trans
        self.clf = clf

    def predict(self, x):
        start = timer()
        x = np.asarray(x, dtype=np.float32)
        zeros = np.zeros((x.shape[0], 1), dtype=np.float32)
        ones = np.ones((x.shape[0], 1), dtype=np.float32)
        x = np.concatenate([
            np.concatenate([x, zeros], axis=1),
            np.concatenate([x, ones], axis=1)
        ],
                           axis=0)
        y = self.clf.predict(x)
        y = self.y_transform.inverse_transform(y.reshape(-1, 1)).reshape(-1)
        _, y = self.detention_transform.inverse_transform(x, y)
        end = timer()
        print('Elapsed time:', end - start, file=sys.stderr)
        return np.maximum(y, 0)


def quantize(y, steps):
    res = np.zeros(y.shape)
    for s in steps:
        res[y >= s] += 1
    return res


def loc_compress(loc):
    INDICES = [0] * 6 + [1] * 4 + [2] * 3 + [3] * 3 + [4] * 2 + [5] * 2
    results = [0 for i in range(6)]
    for i, x in enumerate(loc):
        if x != '0':
            results[INDICES[i]] = 1
    assert sum(results) == 1
    return results


def parse_file(filename):
    detention_py = []
    features_py = []

    LOC_HEADERS = ['臺北區', '臺中區', '臺南區', '高雄區', '花蓮區', '金門區']

    with open(filename, encoding='utf-8') as csvfile:
        for i, row in enumerate(csv.reader(csvfile)):
            if i == 0:
                headers = LOC_HEADERS + row[24:127] + [
                    'is_simple', 'year', 'is_detention'
                ]
                continue
            detention_py.append(float(row[3]))
            if row[27] == '':  # judge1
                row[27] = '-1'
            year = int(row[0][-15:-11])
            is_simple = '簡' in row[0] or '易' in row[0]
            is_detention = row[1] != '0'
            feature_raw = loc_compress(
                row[4:24]) + row[24:127] + [is_simple, year, is_detention]
            feature = list(map(float, feature_raw))
            assert len(headers) == len(feature)
            features_py.append(feature)

    detention = np.asarray(detention_py, dtype=np.float32)
    features = np.asarray(features_py, dtype=np.float32)

    print('Raw feature shape:', features.shape, detention.shape)
    return headers, sk.utils.shuffle(features, detention, random_state=42)


def xgb_search(x, y):
    # pylint: disable=unreachable
    params = {
        'colsample_bytree': 0.5554998889986928,
        'eta': 0.013269452584705852,
        'lambda': 0.860536381309151,
        'max_depth': 8,
        'n_estimators': 1000,
        'subsample': 0.7997240407941067
    }
    return params

    params_grid = {
        'n_estimators': list(range(100, 2000, 100)),
        'eta': sk.utils.fixes.loguniform(0.01, 0.3),
        'max_depth': [4, 5, 6, 7, 8],
        'lambda': sk.utils.fixes.loguniform(0.1, 100),
        'colsample_bytree': scipy.stats.uniform(0, 1),
        'subsample': scipy.stats.uniform(0, 1)
    }
    scorer = sk.metrics.make_scorer(real_mse_loss)
    clf = xgb.XGBRegressor(n_jobs=1)
    gs = sk.model_selection.RandomizedSearchCV(clf,
                                               params_grid,
                                               scoring=scorer,
                                               verbose=100,
                                               n_iter=3000,
                                               n_jobs=12)
    gs.fit(x, y)
    pprint.pprint(gs.cv_results_)
    pprint.pprint(gs.best_score_)
    pprint.pprint(gs.best_params_)

    with open('xgb.cfg', 'w', encoding='utf-8') as f:
        f.write(str(gs.best_params_))
    return gs.best_params_


def xgb_search_hyperopt(x, y):
    # pylint: disable=unreachable
    return xgb.XGBRegressor(base_score=0.5,
                            booster='gbtree',
                            colsample_bylevel=0.9722226955015091,
                            colsample_bynode=1,
                            colsample_bytree=0.8182852603370665,
                            gamma=0.00025590851098164627,
                            gpu_id=-1,
                            importance_type='gain',
                            interaction_constraints='',
                            learning_rate=0.0010517851814402747,
                            max_delta_step=0,
                            max_depth=10,
                            min_child_weight=1,
                            missing=np.nan,
                            monotone_constraints='()',
                            n_estimators=1200,
                            n_jobs=-1,
                            num_parallel_tree=1,
                            objective='reg:linear',
                            random_state=4,
                            reg_alpha=0.02005910842264448,
                            reg_lambda=1.0761161508945778,
                            scale_pos_weight=1,
                            seed=4,
                            subsample=0.5685065771035096,
                            tree_method='exact',
                            validate_parameters=1,
                            verbosity=None)

    clf = hpsklearn.HyperoptEstimator(
        regressor=hpsklearn.xgboost_regression('xgb'),
        algo=hyperopt.tpe.suggest,
        max_evals=2000,
        loss_fn=lambda y_t, y_p: -real_mse_loss(y_t, y_p),
        fit_increment_dump_filename='xgb.pkl',
        n_jobs=-1)
    clf.fit(x, y)
    print(clf.best_model())
    return clf.best_model()['learner']


def adaboost_search_hyperopt(x, y):
    # pylint: disable=unreachable
    return sk.pipeline.make_pipeline(
        sk.preprocessing.Normalizer(norm='l1'),
        sk.ensemble.AdaBoostRegressor(learning_rate=0.00649408978085508,
                                      loss='exponential',
                                      n_estimators=381,
                                      random_state=1))

    clf = hpsklearn.HyperoptEstimator(
        regressor=hpsklearn.ada_boost_regression('adaboost'),
        algo=hyperopt.tpe.suggest,
        max_evals=3000,
        loss_fn=lambda y_t, y_p: -real_mse_loss(y_t, y_p),
        fit_increment_dump_filename='adaboost.pkl',
        n_jobs=-1)
    clf.fit(x, y)
    print(clf.best_model())
    return clf.best_model()['learner']


def lightgbm_search_hyperopt(x, y):
    # pylint: disable=unreachable
    return sk.pipeline.make_pipeline(
        sk.decomposition.PCA(n_components=76, whiten=True),
        lgbm.LGBMRegressor(colsample_bytree=0.6065112760801376,
                           learning_rate=0.013465906723706974,
                           max_delta_step=0,
                           max_depth=9,
                           min_child_weight=6,
                           n_estimators=1200,
                           num_leaves=86,
                           objective='regression',
                           reg_alpha=3.265821353925552e-05,
                           reg_lambda=1.614203398691168,
                           scale_pos_weight=1,
                           seed=1,
                           subsample=0.5623913320786917))

    clf = hpsklearn.HyperoptEstimator(
        regressor=hpsklearn.lightgbm_regression('lightgbm'),
        algo=hyperopt.tpe.suggest,
        max_evals=3000,
        loss_fn=lambda y_t, y_p: -real_mse_loss(y_t, y_p),
        fit_increment_dump_filename='lightgbm.pkl',
        n_jobs=-1)
    clf.fit(x, y)
    print(clf.best_model())
    return clf.best_model()['learner']


def extratrees_search_hyperopt(x, y):
    # pylint: disable=unreachable
    return sk.pipeline.make_pipeline(
        sk.decomposition.PCA(n_components=76),
        sk.ensemble.ExtraTreesRegressor(bootstrap=True,
                                        max_features=None,
                                        n_estimators=678,
                                        n_jobs=-1,
                                        random_state=0,
                                        verbose=False))

    clf = hpsklearn.HyperoptEstimator(
        regressor=hpsklearn.extra_trees_regression('extratrees'),
        algo=hyperopt.tpe.suggest,
        max_evals=3000,
        loss_fn=lambda y_t, y_p: -real_mse_loss(y_t, y_p),
        fit_increment_dump_filename='extratrees.pkl',
        n_jobs=-1)
    clf.fit(x, y)
    print(clf.best_model())
    return clf.best_model()['learner']


def huber_search(x, y):
    # pylint: disable=unreachable
    params = {
        'alpha': 0.17104713146368217,
        'epsilon': 1.8615978272378102,
        'max_iter': 1000
    }
    return params

    params_grid = {
        'epsilon': sk.utils.fixes.loguniform(1.00001, 2),
        'alpha': sk.utils.fixes.loguniform(1e-6, 1),
        'max_iter': [1000]
    }
    scorer = sk.metrics.make_scorer(real_mse_loss)
    clf = sk.linear_model.HuberRegressor()
    gs = sk.model_selection.RandomizedSearchCV(clf,
                                               params_grid,
                                               scoring=scorer,
                                               verbose=100,
                                               n_iter=3000,
                                               n_jobs=12)
    gs.fit(x, y)
    pprint.pprint(gs.cv_results_)
    pprint.pprint(gs.best_score_)
    pprint.pprint(gs.best_params_)

    with open('huber.cfg', 'w', encoding='utf-8') as f:
        f.write(str(gs.best_params_))
    return gs.best_params_


def kernel_search(x, y):
    # pylint: disable=unreachable
    params = {
        'alpha': 0.0033567993323529387,
        'gamma': 2.8508544686111307e-05,
        'kernel': 'rbf'
    }
    return params

    params_grid = {
        'kernel': ['rbf'],
        'alpha': sk.utils.fixes.loguniform(1e-6, 100),
        'gamma': sk.utils.fixes.loguniform(1e-6, 100),
    }
    scorer = sk.metrics.make_scorer(real_mse_loss)
    clf = sk.kernel_ridge.KernelRidge()
    gs = sk.model_selection.RandomizedSearchCV(clf,
                                               params_grid,
                                               scoring=scorer,
                                               verbose=100,
                                               n_iter=3000,
                                               n_jobs=12)
    gs.fit(x, y)
    pprint.pprint(gs.cv_results_)
    pprint.pprint(gs.best_score_)
    pprint.pprint(gs.best_params_)

    with open('kern.cfg', 'w', encoding='utf-8') as f:
        f.write(str(gs.best_params_))
    return gs.best_params_


def poly_search(x, y):
    # pylint: disable=unreachable
    params = {'regress__alpha': 58.416728280690556}
    return params

    params_grid = {
        'regress__alpha': sk.utils.fixes.loguniform(1e-6, 100),
    }
    scorer = sk.metrics.make_scorer(real_mse_loss)
    clf = sk.pipeline.Pipeline([
        ('transform', sk.preprocessing.PolynomialFeatures(degree=2)),
        ('regress', sk.linear_model.Ridge())
    ])
    gs = sk.model_selection.RandomizedSearchCV(clf,
                                               params_grid,
                                               scoring=scorer,
                                               verbose=100,
                                               n_iter=100,
                                               n_jobs=12)
    gs.fit(x, y)
    pprint.pprint(gs.cv_results_)
    pprint.pprint(gs.best_score_)
    pprint.pprint(gs.best_params_)

    with open('poly.cfg', 'w', encoding='utf-8') as f:
        f.write(str(gs.best_params_))
    return gs.best_params_


def elastic_search(x, y):
    # pylint: disable=unreachable
    params = {'regress__l1_ratio': 0.024690704618528736}
    return params

    params_grid = {
        'regress__l1_ratio': scipy.stats.uniform(0, 1),
    }
    scorer = sk.metrics.make_scorer(real_mse_loss)
    clf = sk.pipeline.Pipeline([
        ('transform', sk.preprocessing.PolynomialFeatures(degree=2)),
        ('regress', sk.linear_model.ElasticNetCV(n_jobs=12, max_iter=100000))
    ])
    gs = sk.model_selection.RandomizedSearchCV(clf,
                                               params_grid,
                                               scoring=scorer,
                                               verbose=100,
                                               n_iter=100,
                                               n_jobs=12)
    gs.fit(x, y)
    pprint.pprint(gs.cv_results_)
    pprint.pprint(gs.best_score_)
    pprint.pprint(gs.best_params_)

    with open('elastic.cfg', 'w', encoding='utf-8') as f:
        f.write(str(gs.best_params_))
    return gs.best_params_


def knn_search(x, y):
    # pylint: disable=unreachable
    params = {'n_neighbors': 80, 'p': 2, 'weights': 'distance'}
    return params

    params_grid = {
        'n_neighbors': list(range(30, 500, 10)),
        'p': [1, 2],
        'weights': ['uniform', 'distance']
    }

    scorer = sk.metrics.make_scorer(real_mse_loss)
    clf = sk.neighbors.KNeighborsRegressor()
    gs = sk.model_selection.GridSearchCV(clf,
                                         params_grid,
                                         scoring=scorer,
                                         verbose=100,
                                         n_jobs=12)
    gs.fit(x, y)
    pprint.pprint(gs.cv_results_)
    pprint.pprint(gs.best_score_)
    pprint.pprint(gs.best_params_)

    with open('knn.cfg', 'w', encoding='utf-8') as f:
        f.write(str(gs.best_params_))
    return gs.best_params_


def forest_search_hyperopt(x, y):
    # pylint: disable=unreachable
    return sk.pipeline.make_pipeline(
        sk.preprocessing.Normalizer(norm='l1'),
        sk.ensemble.RandomForestRegressor(max_features=0.1334400641443514,
                                          n_estimators=3000,
                                          n_jobs=-1,
                                          random_state=1,
                                          verbose=False))

    clf = hpsklearn.HyperoptEstimator(
        regressor=hpsklearn.random_forest_regression('forest'),
        algo=hyperopt.tpe.suggest,
        max_evals=3000,
        loss_fn=lambda y_t, y_p: -real_mse_loss(y_t, y_p),
        fit_increment_dump_filename='forest.pkl',
        n_jobs=-1)
    clf.fit(x, y)
    print(clf.best_model())
    return clf.best_model()['learner']


def tabnet_search(x, y):
    # pylint: disable=unreachable
    params = {
        'gamma': 1.0,
        'lambda_sparse': 0.0002137072091715285,
        'mask_type': 'entmax',
        'n_d': 48,
        'n_a': 48,
        'n_independent': 5,
        'n_shared': 1,
        'n_steps': 4,
        'scheduler_params': dict(mode="min",
                                 patience=7,
                                 min_lr=1e-5,
                                 factor=0.5),
        'scheduler_fn': torch.optim.lr_scheduler.ReduceLROnPlateau
    }
    return params

    split = sk.model_selection.KFold()
    train_idx, valid_idx = next(iter(split.split(x)))
    valid_x, valid_y = x[valid_idx], y[valid_idx]
    train_x, train_y = x[train_idx], y[train_idx]

    feature_pipeline = sk.pipeline.Pipeline([
        ('custom', CustomFeatureTransform()),
        ('imputer', sk.impute.SimpleImputer())
    ])
    feature_pipeline.fit(train_x, train_y)
    valid_x = feature_pipeline.transform(valid_x)

    def objective(trial):  # pylint: disable=unused-variable
        mask_type = trial.suggest_categorical('mask_type',
                                              ['entmax', 'sparsemax'])
        n_da = trial.suggest_int('n_da', 4, 64, step=2)
        n_steps = trial.suggest_int('n_steps', 1, 10, step=1)
        gamma = trial.suggest_float('gamma', 1., 1.5, step=0.1)
        n_independent = trial.suggest_int('n_independent', 1, 5)
        n_shared = trial.suggest_int('n_shared', 1, 5)
        lambda_sparse = trial.suggest_float('lambda_sparse', 1e-6, 1, log=True)
        scorer = sk.metrics.make_scorer(real_mse_loss)
        clf = TabNetRegressor()
        clf = sk.pipeline.Pipeline([
            ('transform', CustomFeatureTransform()),
            ('impute', sk.impute.SimpleImputer()),
            ('model', clf),
        ])
        return np.mean(
            sk.model_selection.cross_val_score(
                clf,
                train_x,
                train_y,
                scoring=scorer,
                fit_params={
                    'model__patience': 50,
                    'model__eval_set': [(valid_x, valid_y.reshape(-1, 1))],
                    'model__init_kwargs': {
                        'mask_type':
                        mask_type,
                        'n_d':
                        n_da,
                        'n_a':
                        n_da,
                        'n_steps':
                        n_steps,
                        'gamma':
                        gamma,
                        'n_independent':
                        n_independent,
                        'n_shared':
                        n_shared,
                        'lambda_sparse':
                        lambda_sparse,
                        'scheduler_params':
                        dict(mode="min", patience=7, min_lr=1e-5, factor=0.5),
                        'scheduler_fn':
                        torch.optim.lr_scheduler.ReduceLROnPlateau
                    }
                }))

    study = optuna.create_study(direction='maximize',
                                study_name='TabNet optimization',
                                storage="sqlite:///tabnet-optuna.db",
                                load_if_exists=True)
    study.optimize(objective, timeout=5 * 60 * 60)
    print(study.best_params)
    params = study.best_params
    params['n_d'] = params['n_da']
    params['n_a'] = params['n_da']
    del params['n_da']
    return params


def get_clf(data_headers, train_x, transformed_x, train_y):
    # pylint: disable=too-many-locals
    clf_xgb1 = xgb.XGBRegressor(verbosity=1,
                                n_jobs=1,
                                **xgb_search(transformed_x, train_y))

    clf_xgb2 = xgb_search_hyperopt(train_x, train_y)

    clf_ada = adaboost_search_hyperopt(train_x, train_y)

    clf_lightgbm = lightgbm_search_hyperopt(train_x, train_y)

    clf_extratrees = extratrees_search_hyperopt(train_x, train_y)

    # Old MLP model
    # torch_net = torch.nn.Sequential(torch.nn.Linear(len(data_headers), 64),
    #                                 torch.nn.ReLU(),
    #                                 torch.nn.BatchNorm1d(64),
    #                                 torch.nn.Linear(64, 64),
    #                                 torch.nn.ReLU(),
    #                                 torch.nn.Linear(64, 1),
    #                                 torch.nn.Softplus())
    torch_net = torch.nn.Sequential(torch.nn.Linear(len(data_headers), 256),
                                    torch.nn.ReLU(), torch.nn.Dropout(0.5),
                                    torch.nn.Unflatten(1, (16, 16)),
                                    torch.nn.BatchNorm1d(16),
                                    torch.nn.Conv1d(16, 32, 3, padding='same'),
                                    torch.nn.ReLU(), torch.nn.BatchNorm1d(32),
                                    torch.nn.Dropout(0.5),
                                    torch.nn.Conv1d(32, 32, 3, padding='same'),
                                    torch.nn.ReLU(), torch.nn.AvgPool1d(2),
                                    torch.nn.BatchNorm1d(32),
                                    torch.nn.Dropout(0.5),
                                    torch.nn.Conv1d(32, 32, 3, padding='same'),
                                    torch.nn.ReLU(), torch.nn.AvgPool1d(2),
                                    torch.nn.BatchNorm1d(32),
                                    torch.nn.Dropout(0.5),
                                    torch.nn.Conv1d(32, 32, 3, padding='same'),
                                    torch.nn.ReLU(), torch.nn.AvgPool1d(2),
                                    torch.nn.BatchNorm1d(32),
                                    torch.nn.Dropout(0.5),
                                    torch.nn.Conv1d(32, 64, 3, padding='same'),
                                    torch.nn.ReLU(), torch.nn.AvgPool1d(2),
                                    torch.nn.Flatten(), torch.nn.Dropout(0.2),
                                    torch.nn.BatchNorm1d(64),
                                    torch.nn.Linear(64, 64), torch.nn.ReLU(),
                                    torch.nn.Dropout(0.5),
                                    torch.nn.Linear(64, 1),
                                    torch.nn.Softplus())

    clf_mlp = NNRegressor(torch_net,
                          lr=0.0003,
                          batch_size=32,
                          optimizer=torch.optim.AdamW,
                          optimizer__weight_decay=0.01,
                          max_epochs=1000,
                          train_split=skorch.dataset.CVSplit(5),
                          callbacks=[
                              skorch.callbacks.EarlyStopping(patience=200),
                              skorch.callbacks.GradientNormClipping(1),
                          ])

    clf_kernel = sk.kernel_ridge.KernelRidge(
        **kernel_search(transformed_x, train_y))

    clf_poly = sk.pipeline.Pipeline([
        ('transform', sk.preprocessing.PolynomialFeatures(degree=2)),
        ('regress', sk.linear_model.ElasticNetCV(max_iter=50000, n_jobs=3))
    ])
    clf_poly.set_params(**elastic_search(transformed_x, train_y))

    clf_catboost = catboost.CatBoostRegressor(n_estimators=1000)

    clf_tn = TabNetRegressor(tabnet_search(transformed_x, train_y))

    clf_knn = sk.neighbors.KNeighborsRegressor(
        **knn_search(transformed_x, train_y))

    clf_forest = forest_search_hyperopt(train_x, train_y)

    clf_att_forest = sk.pipeline.make_pipeline(
        AttitudeTransform(),
        sk.ensemble.RandomForestRegressor(n_estimators=1000))

    clf_tpot = sk.pipeline.make_pipeline(
        StackingEstimator(
            estimator=sk.linear_model.SGDRegressor(alpha=0.001,
                                                   eta0=0.1,
                                                   fit_intercept=True,
                                                   l1_ratio=0.0,
                                                   learning_rate="invscaling",
                                                   loss="huber",
                                                   penalty="elasticnet",
                                                   power_t=0.0)),
        StackingEstimator(
            estimator=sk.linear_model.SGDRegressor(alpha=0.01,
                                                   eta0=0.1,
                                                   fit_intercept=False,
                                                   l1_ratio=0.75,
                                                   learning_rate="constant",
                                                   loss="epsilon_insensitive",
                                                   penalty="elasticnet",
                                                   power_t=0.0)),
        StackingEstimator(
            estimator=sk.svm.LinearSVR(C=0.1,
                                       dual=True,
                                       epsilon=0.1,
                                       loss="epsilon_insensitive",
                                       tol=0.0001)),
        StackingEstimator(estimator=sk.neighbors.KNeighborsRegressor(
            n_neighbors=41, p=1, weights="uniform")),
        sk.ensemble.RandomForestRegressor(bootstrap=True,
                                          max_features=0.3,
                                          min_samples_leaf=2,
                                          min_samples_split=3,
                                          n_estimators=3000))

    clf_final = sk.pipeline.Pipeline([
        ('transform', FeatureStackTransform(len(data_headers))),
        ('regress',
         sk.linear_model.RidgeCV(alphas=[10**i for i in range(-9, 9)]))
    ])

    clf = sk.ensemble.StackingRegressor([
        ('xgb1', clf_xgb1),
        ('xgb2', clf_xgb2),
        ('ada', clf_ada),
        ('lightgbm', clf_lightgbm),
        ('extratrees', clf_extratrees),
        ('mlp', clf_mlp),
        ('kernelridge', clf_kernel),
        ('poly', clf_poly),
        ('catboost', clf_catboost),
        ('tabnet', clf_tn),
        ('knn', clf_knn),
        ('forest', clf_forest),
        ('att_forest', clf_att_forest),
        ('tpot', clf_tpot),
    ],
                                        final_estimator=clf_final,
                                        passthrough=True,
                                        verbose=1,
                                        n_jobs=12)

    # _, _, _, clf = load_predictor('predictor.pkl')

    clf_autogluon = AutogluonRegressor(
        TabularPredictor(label='money',
                         path='autogluon',
                         eval_metric=autogluon.core.metrics.make_scorer(
                             'Real MSE', real_mse_loss_df)), data_headers)
    clf_autogluon = FixedRegressor(
        AutogluonRegressor(TabularPredictor.load(path='autogluon'),
                           data_headers))
    clf_autogluon.clf.clf.leaderboard()
    _ = clf_autogluon

    clf_mljar = AutoML(data_headers=data_headers,
                       mode='Optuna',
                       optuna_time_budget=3 * 60 * 60,
                       results_path='results-explain-full',
                       stack_models=True,
                       explain_level=2,
                       golden_features=True,
                       features_selection=True,
                       hill_climbing_steps=3,
                       top_models_to_improve=5,
                       boost_on_errors=True,
                       kmeans_features=True,
                       eval_metric='mse')
    clf_mljar = FixedRegressor(clf_mljar)
    # clf_mljar.fit(pd.DataFrame(transformed_x, columns=data_headers), train_y)

    clf_autosk = autosklearn.regression.AutoSklearnRegressor(
        time_left_for_this_task=18 * 60 * 60, memory_limit=4096, n_jobs=-1)
    with open('predictor-autosklearn.pkl', 'rb') as pf:
        clf_autosk = FixedRegressor(pickle.load(pf).clf['model'])
    _ = clf_autosk

    clf_dummy = sk.dummy.DummyRegressor()
    _ = clf_dummy

    clf = sk.pipeline.Pipeline([
        ('transform', CustomFeatureTransform()),
        ('impute', sk.impute.SimpleImputer()),
        ('model', clf),
    ])

    clf_all = FixedStackingRegressor([
        ('self', FixedRegressor(clf['model'])),
        ('autogluon', clf_autogluon),
        ('mljar', clf_mljar),
        ('autosk', clf_autosk),
    ],
                                     final_estimator=sk.linear_model.RidgeCV())
    clf_all = sk.pipeline.Pipeline([
        ('transform', FixedRegressor(clf['transform'])),
        ('impute', FixedRegressor(clf['impute'])),
        ('model', clf_all),
    ])

    return clf


def permute_features(train_x, test_x):
    shuffle_none: typing.List[int] = []
    shuffle_judge = list(range(10)) + [109, 110]
    shuffle_bg = list(range(10, 29))
    _ = shuffle_none, shuffle_judge, shuffle_bg
    for idx in shuffle_none:
        train_x[:, idx] = train_x[np.random.permutation(train_x.shape[0]), idx]
        test_x[:, idx] = test_x[np.random.permutation(test_x.shape[0]), idx]

    return train_x, test_x


def explore_plot_kmeans(train_x):
    '''Visualize K-means.'''
    kmeans = sk.cluster.KMeans(4)
    pca = sk.decomposition.PCA(3)
    kmeans_labels = kmeans.fit_predict(train_x)
    pca_labels = pca.fit_transform(kmeans.transform(train_x))
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.scatter(pca_labels[:, 0],
               pca_labels[:, 1],
               pca_labels[:, 2],
               c=kmeans_labels,
               cmap='RdBu')
    plt.show()


def explore_kmeans_silhouette(train_x):
    '''Select number of K-means clusters with silhouette scores.'''
    for i in range(2, 64):
        cluster = sk.cluster.KMeans(i)
        cluster_labels = cluster.fit_predict(train_x)
        silhouette_avg = sk.metrics.silhouette_score(train_x, cluster_labels)
        print(f'Silhouette Score with n = {i}:', silhouette_avg)


def explore_permutation_importance(headers, clf, x, y):
    r = sk.inspection.permutation_importance(clf, x, y)
    importances = list(zip(headers, r.importances_mean))
    importances.sort(key=lambda x: x[1])
    return importances


def explore_print_feature_scatter(headers, train_x, transformed_x, train_y):
    '''Plot feature scatter plot.'''
    _ = train_x  # Unused

    idx = headers.index('recidivism')
    z = np.polyfit(transformed_x[:, idx], train_y, 1)
    p = np.poly1d(z)
    plt.plot(transformed_x[:, idx], p(transformed_x[:, idx]))
    plt.scatter(transformed_x[:, idx], train_y)
    plt.show()


def explore_print_feature_vif(headers, train_x, transformed_x, train_y):
    '''Print feature colinearity.'''
    _ = train_x, train_y  # Unused

    print('Feature VIF:', [(h, variance_inflation_factor(transformed_x, i))
                           for i, h in enumerate(headers)])


def explore_print_feature_correlation(headers, train_x, transformed_x,
                                      train_y):
    _ = train_x  # Unused

    print(
        'Feature F-scores:',
        list(
            zip(headers,
                *sk.feature_selection.f_regression(transformed_x, train_y))))

    print(
        'Feature Mutual Information:',
        list(
            zip(
                headers,
                sk.feature_selection.mutual_info_regression(
                    transformed_x, train_y))))

    feat_indices: typing.List[int] = []
    feat_tscore: typing.List[typing.Tuple[str, float]] = []
    for i, head in enumerate(headers):
        vec = transformed_x[:, i]
        if np.max(vec) - np.min(vec) > 1.01:
            continue
        pval = scipy.stats.ttest_ind(train_y[vec > 0.5],
                                     train_y[vec < 0.5]).pvalue
        feat_tscore.append((head, pval))
        if pval < 0.1:
            feat_indices.append(i)
    print('Features with T-test p < 0.1:', np.asarray(headers)[feat_indices])
    print('Feature T-test p:', feat_tscore)


def explore_plot_shap_scatter(clf, headers, train_x, transformed_x, train_y):
    _ = train_x, train_y  # Unused

    explainer = shap.TreeExplainer(clf)
    shap_values = explainer(pd.DataFrame(transformed_x, columns=headers))
    shap.plots.beeswarm(shap_values)


def load_predictor(fn):
    with open(fn, 'rb') as f:
        predictor = pickle.load(f)
        # pylint: disable=no-member
        detention_transform = predictor.detention_transform
        y_trans = predictor.y_transform
        clf = predictor.clf
        print('Loaded predictor:', clf)

    return predictor, detention_transform, y_trans, clf


def print_quantized_accuracy(headers, xx, yy, y_pred):
    '''Quantize predictions and treat as classification problem.'''
    _ = headers  # Unused

    detention_indices = xx[:, -1] > 0.5
    xx = xx[detention_indices]
    yy = yy[detention_indices]
    y_pred = y_pred[detention_indices]

    steps = [np.percentile(yy, i) for i in range(25, 100, 25)]
    yy = quantize(yy, steps)
    y_pred = quantize(y_pred, steps)

    print(sk.metrics.accuracy_score(yy, y_pred))


def print_loss(headers, xx, yy, y_pred, detention_money_ratio):
    _ = headers  # Unused

    detention_indices = xx[:, -1] > 0.5
    for x, y, yp in [(xx[detention_indices], yy[detention_indices],
                      y_pred[detention_indices]),
                     (xx[~detention_indices], yy[~detention_indices],
                      y_pred[~detention_indices]), (xx, yy, y_pred)]:
        y = y.reshape(-1)
        yp = yp.reshape(-1)

        loss = yp - y
        rel = np.abs(loss / y)
        ratio = (y - yp) / yp

        print('MAE:', np.mean(np.abs(loss)))
        print('MSE:', np.mean(loss**2))
        print('MAPE:', np.mean(rel))
        print('R:', scipy.stats.pearsonr(y, yp))
        print('R2:', sk.metrics.r2_score(y, yp))
        print('Ratio of MAE <= $2500:', sum(np.abs(loss) <= 2500) / len(loss))
        print(
            'Ratio of MAE <= 12 days:',
            sum(np.abs(loss) * detention_money_ratio <= 12 * 1000) / len(loss))
        print('Ratio of MAPE under 50%:', len(rel[rel < 0.5]) / len(rel))
        print('Ratio 0.5~2:',
              len(ratio[(ratio <= 1) & (ratio >= -0.5)]) / len(ratio))
        print()

        if 'matplotlib.pyplot' not in sys.modules:
            continue

        _, ax = plt.subplots(4)
        ax[0].hist(loss, histtype='step', bins=20)
        ax[1].hist(np.abs(loss),
                   cumulative=True,
                   density=True,
                   histtype='step',
                   bins=50)
        ax[2].hist(loss / y, histtype='step', bins=20, range=(-1, 3))
        ax[3].hist(np.exp(np.abs(np.log(1 + (y - yp) / yp))),
                   cumulative=True,
                   density=True,
                   histtype='step',
                   bins=50,
                   range=(1, 5))
        plt.show()
        plt.close()

        lim = 15000
        plt.xlim(0, lim)
        plt.ylim(0, lim)
        plt.scatter(yp[x[:, -1] > 0.5], y[x[:, -1] > 0.5], s=8, alpha=0.5)
        plt.scatter(yp[x[:, -1] < 0.5], y[x[:, -1] < 0.5], s=8, alpha=0.5)
        plt.plot([0, lim], [0, lim], color='r')
        plt.plot([0, lim], [0, 2.0 * lim], color='r')
        plt.plot([0, lim], [0, 0.5 * lim], color='r')
        plt.show()
        plt.close()


def main():
    # pylint: disable=too-many-locals
    global y_transform

    data_headers, (train_x, train_y) = parse_file('train-new.csv')
    data_headers_test, (test_x, test_y) = parse_file('test-new.csv')
    assert data_headers == data_headers_test
    del data_headers_test

    data_headers.pop()
    data_headers += [
        'cases_notfound', 'cases_zero', 'num_words', 'level_avg', 'level_std',
        'loc_avg', 'loc_std', 'bad_count', 'good_count', 'is_detention'
    ]
    print('Features: ', list(enumerate(data_headers)))

    train_x, test_x = permute_features(train_x, test_x)

    detention_transform = DetentionTransform().fit(train_x, train_y)
    train_x, train_y = detention_transform.transform(train_x, train_y)
    test_x, test_y = detention_transform.transform(test_x, test_y)

    # Remove outliers from training set
    train_x, train_y = train_x[train_y <= 15000], train_y[train_y <= 15000]

    y_transform = sk.preprocessing.PowerTransformer(method='box-cox')
    y_transform.fit(train_y.reshape(-1, 1))
    train_y = y_transform.transform(train_y.reshape(-1, 1)).reshape(-1)
    test_y = y_transform.transform(test_y.reshape(-1, 1)).reshape(-1)

    feature_pipeline = sk.pipeline.Pipeline([
        ('custom', CustomFeatureTransform()),
        ('imputer', sk.impute.SimpleImputer())
    ])
    transformed_x = feature_pipeline.fit_transform(train_x, train_y)

    explore_print_feature_correlation(data_headers, train_x, transformed_x,
                                      train_y)

    # clf = get_clf(data_headers, train_x, transformed_x, train_y)

    _, detention_transform, y_transform, clf = load_predictor(
        'predictor.pkl')

    # Cross-validation
    # xx = train_x
    # yy = train_y
    # y_pred = sk.model_selection.cross_val_predict(clf,
    #                                               train_x,
    #                                               train_y,
    #                                               n_jobs=4,
    #                                               verbose=1)

    # Split test for L2 stacking
    # train_x, test_x, train_y, test_y = sk.model_selection.train_test_split(
    #     test_x, test_y, test_size=0.5, random_state=42)

    # Evaluate on validation set
    # split = sk.model_selection.KFold()
    # train_idx, valid_idx = next(iter(split.split(train_x)))
    # valid_x, valid_y = train_x[valid_idx], train_y[valid_idx]
    # train_x, train_y = train_x[train_idx], train_y[train_idx]
    # xx = valid_x
    # yy = valid_y

    # Evaluate on test set
    xx = test_x
    yy = test_y

    # clf.fit(train_x, train_y)

    # Initialize Predictor object and test prediction time
    # Run twice so that things are properly cached the second time
    predictor = Predictor(detention_transform, y_transform, clf)
    predictor.predict(test_x[:1, :-1])
    predictor.predict(test_x[:1, :-1])

    # Save trained model
    # with open('predictor-all.pkl', 'wb') as f:
    #     pickle.dump(predictor, f)

    y_pred = clf.predict(xx)

    yy = y_transform.inverse_transform(yy.reshape(-1, 1)).reshape(-1)
    y_pred = y_transform.inverse_transform(y_pred.reshape(-1, 1)).reshape(-1)

    detention_money_ratio = \
        detention_transform.money_std / detention_transform.detention_std
    print_loss(data_headers, xx, yy, y_pred, detention_money_ratio)


if __name__ == '__main__':
    torch.manual_seed(42)
    main()
