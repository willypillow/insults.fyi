# pylint: disable=missing-module-docstring,missing-function-docstring
import collections
import csv
import datetime
import json
import re
import typing

import cloudpickle as pickle
import numpy as np

from django.http import HttpResponse
from django.template import loader
from django.utils.html import escape

with open('words.json', 'r', encoding='utf-8') as words_file:
    WORD_MAP = json.load(words_file)

WORD_MAP_LIST = WORD_MAP.items()

REVERSE_WORD_MAP = {}
for i, (_, keywords) in enumerate(WORD_MAP_LIST):
    for k in keywords:
        REVERSE_WORD_MAP[k] = i

REVERSE_WORDS = sorted(REVERSE_WORD_MAP.items(), key=lambda x: -len(x[0]))
REVERSE_WORDS_REGEX = []

for k, word_type in REVERSE_WORDS:
    regex = re.compile(k, re.IGNORECASE)
    REVERSE_WORDS_REGEX.append((regex, k, word_type))

KEYWORDS = WORD_MAP.keys()
KEYWORDS_COUNT: typing.Dict[str, int] = collections.defaultdict(int)

with open('model/train-new.csv', 'r', encoding='utf-8') as train_file:
    reader = csv.DictReader(train_file)
    for row in reader:
        for k in KEYWORDS:
            if row[k].strip() != '0':
                KEYWORDS_COUNT[k] += 1

with open('predictor.pkl', 'rb') as predictor_file:
    predictor = pickle.load(predictor_file)

with open('judges.json', 'r', encoding='utf-8') as judge_file:
    judge_years = json.load(judge_file)


def submit(request):
    # pylint: disable=too-many-locals
    comment = ''

    is_internet = int(request.GET.get('is-internet', 0))

    past_cases_raw = request.GET.get('past-cases', np.nan)
    try:
        past_cases = float(past_cases_raw if past_cases_raw else np.nan)
    except ValueError:
        if past_cases_raw.strip() in judge_years:
            past_cases = judge_years[past_cases_raw]
        else:
            past_cases = np.nan
            comment += '（資料庫中無此法官資料，總判決數使用預設值）'

    court = int(request.GET.get('court', -1))
    is_simple = request.GET.get('court', -1) == '00'
    court_1hot = [i == court for i in range(3)]
    court_loc = int(request.GET.get('court-loc', -1))
    court_loc_1hot = [i == court_loc for i in range(6)]

    bgs = [int(request.GET.get(f'bg-{i}', 'off') == 'on') for i in range(18)]

    year = request.GET.get('year', datetime.datetime.now().year)

    text = escape(request.GET.get('input-text', ''))

    keywords_1hot = [
        int(any(x in text for x in WORD_MAP[x_type])) for x_type in KEYWORDS
    ]

    for regex, keyword, type_id in REVERSE_WORDS_REGEX:
        text = regex.sub(
            f'<mark class="tooltip" data-tooltip="{type_id}">{keyword}</mark>',
            text)

    for i, (word_type, keywords) in enumerate(WORD_MAP_LIST):
        text = text.replace(
            f'<mark class="tooltip" data-tooltip="{i}">',
            (f'<mark class="tooltip" data-tooltip='
             f'"字眼類別：{word_type}\n過往判決數：{KEYWORDS_COUNT[word_type]}">'))

    if court_loc != -1:
        features = court_loc_1hot + court_1hot + [
            past_cases, is_internet
        ] + bgs + keywords_1hot + [is_simple, year]
        features = [features]
    else:
        features = []
        for i in range(6):
            court_loc_1hot = [j == i for j in range(6)]
            features_sub = court_loc_1hot + court_1hot + [
                past_cases, is_internet
            ] + bgs + keywords_1hot + [is_simple, year]
            features.append(features_sub)

    template = loader.get_template('submit.html')

    if any(x for x in keywords_1hot):
        prediction = predictor.predict(features)  # pylint: disable=no-member
        prediction = np.mean(prediction.reshape(2, -1), axis=1)
        amount, detention_amount = np.round(prediction, -3)
        context = {
            'text': text,
            'amount': int(amount),
            'detention': int(detention_amount // 1000),
            'comment': comment
        }
    else:
        context = {
            'text': text,
            'amount': 0,
            'detention': 0,
            'comment': '未找到具應罰性字眼'
        }

    return HttpResponse(template.render(context, request))
